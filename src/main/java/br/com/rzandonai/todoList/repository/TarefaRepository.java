package br.com.rzandonai.todoList.repository;

import br.com.rzandonai.todoList.model.Tarefa;

import java.util.Comparator;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TarefaRepository  extends  JpaRepository<Tarefa, Long> {
	public default List<Tarefa> findOrdenada(){
		List<Tarefa> lista = findAll();
		lista.sort(Comparator.comparing(Tarefa::getOrdenador));
		return lista;
	}
	
}
