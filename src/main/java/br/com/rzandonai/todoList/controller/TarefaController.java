package br.com.rzandonai.todoList.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.rzandonai.todoList.model.Tarefa;
import br.com.rzandonai.todoList.repository.TarefaRepository;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

@RestController
public class TarefaController {

	    @Autowired
	    private TarefaRepository tarefaRepository;

	    @GetMapping("/tarefas")
	    public List<Tarefa> getQuestions() {
	        return tarefaRepository.findOrdenada();
	    }

	    @PostMapping("/tarefas")
	    public List<Tarefa> createQuestion(@Valid @RequestBody Tarefa tarefa) {
	    	List<Tarefa> lista = tarefaRepository.findOrdenada();
	    	if(lista.size() > 0) {
		    	Tarefa ultimo = lista.get(lista.size() -1);
		    	tarefa.setOrdenador(ultimo.getOrdenador()+1);
	    	}else {
	    		tarefa.setOrdenador(1l);
	    	}
	    	tarefa = tarefaRepository.save(tarefa);
	    	lista.add(tarefa);
	        return lista;
	    }

	    
	    @PutMapping("/tarefas")
	    public List<Tarefa> createQuestion(@Valid @RequestBody List<Tarefa> tarefas) {
	    	for (Tarefa tarefa : tarefas) {
	    		tarefa.setOrdenador(Long.valueOf(tarefas.indexOf(tarefa)));
	    		tarefaRepository.save(tarefa);
			}
	        return tarefaRepository.findOrdenada();
	    }
	    
	    @DeleteMapping("/tarefas/{id}")
	    public List<Tarefa> deleteQuestion(@PathVariable Long id) {
	       Optional<Tarefa> tarefa = tarefaRepository.findById(id);
	       if(tarefa.isPresent()){
	    	   tarefaRepository.delete(tarefa.get());
	       }
	       return tarefaRepository.findOrdenada();
	    }

}
