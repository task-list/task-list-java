package br.com.rzandonai.todoList.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "tarefas")
public class Tarefa extends AuditModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "tarefa_generator")
	@SequenceGenerator(name = "tarefa_generator", sequenceName = "tarefa_sequence", initialValue = 1)
	private Long id;

	@NotBlank
	private String texto;

	private boolean finalizada = false;

	private Long ordenador;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public boolean isFinalizada() {
		return finalizada;
	}

	public void setFinalizada(boolean finalizada) {
		this.finalizada = finalizada;
	}

	public Long getOrdenador() {
		return ordenador;
	}

	public void setOrdenador(Long ordenador) {
		this.ordenador = ordenador;
	}

}
